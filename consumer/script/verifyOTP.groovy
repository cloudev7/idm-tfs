import org.forgerock.json.resource.CreateRequest
import org.forgerock.json.JsonValue
import java.security.SecureRandom

import static Constants.*

class Constants {
  static final String otpField = "otpCode"

  static final String USER_DOES_NOT_EXIST = "User does not exist"
  static final String METHOD_NOT_ALLOWED = "Method Not Allowed"
  static final String OTHER_EXCEPTION = "Other exception"
  static final String INVALID_OTP_CODE = "Invalid OTP Code"
  static final String SUCCESS = "success"
}

def notFound(){
  return new JsonValue([
    "code": 404,
    "reason": USER_DOES_NOT_EXIST,
    "message": USER_DOES_NOT_EXIST,
    "data": []
  ])
}

def notAllowed(){
  return new JsonValue([
    "code": 405,
    "reason": METHOD_NOT_ALLOWED,
    "message": METHOD_NOT_ALLOWED,
    "data": []
  ])
}

def otherError(){
  return new JsonValue([
    "code": 500,
    "reason": OTHER_EXCEPTION,
    "message": OTHER_EXCEPTION,
    "data": []
  ])
}

def invalidOtp(){
  return new JsonValue([
    "code": 500,
    "reason": INVALID_OTP_CODE,
    "message": INVALID_OTP_CODE,
    "data": []
  ])
}

def success(data){
  return new JsonValue([
    "code": 200,
    "status": SUCCESS,
    "data": data
  ])
}

if (request instanceof CreateRequest){

  def reqBody = request.content.getObject()

  if ( null == reqBody || null == reqBody["mail"] )
    return otherError()

  if ( null == reqBody || null == reqBody["otp"] )
    return otherError()

  def mail = (reqBody["mail"]).toString()
  def otpUser = (reqBody["otp"]).toString()

  def filter = [ "_queryFilter" : 'mail eq "' + mail + '"' ]
  def userRepo = openidm.query("managed/user", filter)

  if ( null == userRepo || null == userRepo.result[0] )
    return notFound()
 
  def account = userRepo.result[0] 
  def uid = account["_id"]
  def otpRepo = account[otpField]

  if ( null == otpRepo || otpRepo.trim().isEmpty() )
    return otherError()

  if ( otpRepo == otpUser ){
    def data = [ 
      "userName": account["userName"] 
    ]
    return success(data)
  }
  return invalidOtp()

} else {
  return notAllowed()
}
	
