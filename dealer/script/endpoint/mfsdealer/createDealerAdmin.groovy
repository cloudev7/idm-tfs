import org.forgerock.json.resource.CreateRequest
import org.forgerock.json.JsonValue
import org.forgerock.json.resource.ResourceException
import java.security.SecureRandom
import groovy.json.JsonSlurper

import static Constants.*
import java.util.logging.Logger

class Constants {
  static Logger logger = Logger.getLogger("")

  // BusinessException_22
  static final JsonValue SC_400_REQUIRED_FIELDS_MISSING = [
    "statusCode": "MFA22",
    "message": "Invalid Request. Required field(s) missing or empty"
  ]
  // BusinessException_25
  static final JsonValue SC_409_CONFLICT = [
    "statusCode": "MFA25",
    "message": "Email id already exists"
  ]
  // BusinessException_23
  static final JsonValue SC_422_INVALID_EMAIL = [
    "statusCode": "MFA23",
    "message": "Not a valid email id"
  ]
  // BusinessException_26
  static final JsonValue SC_422_INVALID_CREATOR = [
    "statusCode": "MFA26",
    "message": "Not a valid creator"
  ]
  // BusinessException_24
  static final JsonValue SC_500_OTHER_EXCEPTION = [
    "statusCode": "MFA24",
    "message": "Other Error"
  ]
  // BusinessException_2"
  static final JsonValue SC_405_METHOD_NOT_ALLOWED = [
    "statusCode": "MFA2",
    "message": "Method Not allowed"
  ]
  // BusinessException_21"
  static final JsonValue SC_404_DEALER_NOT_FOUND = [
    "statusCode": "MFA21",
    "message": "Dealer Not Found"
  ]
  static final JsonValue SC_201_CREATED = [
    "code": 201,
    "reason": "Created",
    "detail": [
      "statusCode": "MFA1",
      "message": "Request Completed Successfully"
    ]
  ]

  static final fields = new JsonSlurper().parseText('''[
    { "name": "email",        "required": true},
    { "name": "creator",      "required": true},
    { "name": "isAdmin",      "required": false},
    { "name": "firstName",    "required": false},
    { "name": "lastName",     "required": false},
    { "name": "title",        "required": false},
    { "name": "cellPhone",    "required": false},
    { "name": "roles",        "required": false},
    { "name": "dealerNumber", "required": true}
  ]''');

  static final defaultUserStatus = "Active"
  static final String emailPattern = /[_A-Za-z0-9-]+(.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})/
}

def requiredFieldsMissing(){
  throw new ResourceException(400, "").setDetail(
    new JsonValue(SC_400_REQUIRED_FIELDS_MISSING)
  )
}

def conflictOccured(){
  throw new ResourceException(409, "").setDetail(
    new JsonValue(SC_409_CONFLICT)
  )
}

def invalidEmail(){
  throw new ResourceException(422, "").setDetail(
    new JsonValue(SC_422_INVALID_EMAIL)
  )
}

def invalidCreator(){
  throw new ResourceException(422, "").setDetail(
    new JsonValue(SC_422_INVALID_CREATOR)
  )
}

def otherError(){
  throw new ResourceException(500, "").setDetail(
    new JsonValue(SC_500_OTHER_EXCEPTION)
  )
}

def notAllowed(){
  throw new ResourceException(405, "").setDetail(
    new JsonValue(SC_405_METHOD_NOT_ALLOWED)
  )
}

def dealerNotFound(){
  throw new ResourceException(404, "").setDetail(
    new JsonValue(SC_404_DEALER_NOT_FOUND)
  )
}

def success(){
  return new JsonValue(SC_201_CREATED)
}

def isValidEmail(email){
  return email ==~ emailPattern
}

def isValidCreator(creator){
  if ( creator != "admin" && creator != "corpUser"){
    return false
  }
  return true
}

def emailExists(email){
  def filterCondition = 'email eq "' + email + '"'
  def filter = [ "_queryFilter" : filterCondition ]
  def repoObj = openidm.query("managed/mfsDealerExperiance", filter)

  if ( null == repoObj || null == repoObj.result[0] )
    return false

  return true
}

def isNullOrEmpty(value){
  if (null == value || value.toString().trim().isEmpty()){
    return true
  }
  return false
}

def hasMissingFieldOrValue(payload){
    for (field in fields){
      if ( field["required"] && isNullOrEmpty(payload[field["name"]]) ){
        return true
      }
    }
    return false
}

def getDealerInfo(dealerNumber){
    def filterCondition = 'DealerNumber eq "' + dealerNumber + '"'
    def filter = [ "_queryFilter" : filterCondition ]
    def repoObj = openidm.query("managed/mfsDealerInfo", filter)

    if ( null == repoObj || null == repoObj.result[0] )
      return null
 
    return repoObj.result[0]  
}

if (request instanceof CreateRequest){
  def payload = request.content.getObject()

  if ( null == payload || hasMissingFieldOrValue(payload))
    return requiredFieldsMissing()

  def creator = payload["creator"]

  if (!isNullOrEmpty(creator)){
    if ( !isValidCreator(creator)){
      return invalidCreator()
    }
  }

  def mail = payload["email"]

  if (!isNullOrEmpty(mail)){

    if ( !isValidEmail(mail)){
      return invalidEmail()
    }

    if ( emailExists(mail) ){
      return conflictOccured()
    }
  }
    
  def dealerInfo = null
  try{
    dealerInfo = getDealerInfo(payload["dealerNumber"])
  }catch(Exception e){
      return otherError()
  }

  if ( null == dealerInfo || isNullOrEmpty(dealerInfo["_id"]) ){
    return dealerNotFound()
  }

  def dealerReference = ["_ref":"managed/mfsDealerInfo/" + dealerInfo["_id"]]
  payload.put("dealer", dealerReference)

  def immutableId = UUID.randomUUID().toString()
  payload.put("immutableId", immutableId)
  payload.put("isAdmin", true)
  payload.put("userStatus", defaultUserStatus)

  try{
    def result = openidm.create("managed/mfsDealerExperiance", null, payload)
    return success()

  }catch(Exception e){
    return otherError()
  }

} else {
  return notAllowed()
}

